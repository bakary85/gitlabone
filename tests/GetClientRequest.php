<?php 

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class GetClientRequest extends WebTestCase
{
    private array $serverInformations = [
        'CONTENT_TYPE' => 'application/json',
        'ACCEPT' => 'application/json'
    ];

     public function RequestGetPost(string $method, string $url, string $payload = null) : Response
    {
        $client = self::createClient();
        $client->followRedirects();
         $client->request(
            $method,
            $url,
            [],
            [],
            $this->serverInformations
        );

        return $client->getResponse();
    }
}
