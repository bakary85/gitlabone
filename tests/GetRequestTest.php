<?php

namespace App\Tests;

use App\Entity\Demo;

class GetRequestTest extends GetClientRequest
{

    public function testRequestGet()
    {
        $response =$this->RequestGetPost("GET","/demo");
        $this->assertResponseIsSuccessful();
    }
    
    public function testRequestPostName()
    {
        $payload = new Demo();
        $payload->setName('bakary');
        self::assertEquals('bakary',$payload->getName());

    }
}


